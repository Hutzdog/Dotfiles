# Helper to compile and position all configs
tangle: clean
	for file in ./files/*.org; do \
		emacs $${file} --batch -f org-babel-tangle --kill ; \
	done

install:
	cp -r out/.[!.]* out/* ~ 2> /dev/null || true

REVISION = $(shell git rev-parse --short HEAD)
release: tangle
	cp -r out release
	tar -cJf release-$(REVISION).tar.xz release
	rm -rf release

clean:
	rm -rf out/*
	rm -rf release-*.tar.xz
