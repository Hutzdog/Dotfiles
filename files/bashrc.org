*BashRc*
[[https://codeberg.org/Hutzdog/Dotfiles/raw/branch/main/assets/bash-prompt.png]]
* about
The BASH (Borne Again SHell) shell is the default shell on most UNIX-like systems.

* features
- A few environment variables
- An alias for declarative package management for NixOS
- A prompt config
- Proper history support

* dependencies
- The BASH shell
- NeoVim (for the editor, can be replaced with another editor)
- Rakudo (for the prompt, if you want your own prompt change the value of PS1)
- A font with Powerline support must be used for the prompt (see previous item for prompt override instructions, if you don't have this one change PS2 as well)

* install
1. Install all dependencies
2. Copy [[../out/.bashrc]] file to your user's home
3. (Optional) Copy [[../out/.promptrc.raku]], used to render the prompt. Change PS1 if you don't install this file.

* code
:PROPERTIES:
:header-args:    :tangle ../out/.bashrc
:header-args:sh: :shebang "#!/bin/bash\n"
:END:

** environment variables
This is where we have a few variables such as the editor and history configurations. We also tell Bash to allow the * wildcard to see dotfiles
#+BEGIN_SRC sh :comments org
EDITOR="nvim"
HISTCONTROL=ignoreboth
PATH="$PATH:$HOME/.emacs.distribs/bin:$HOME/.emacs.distribs/distbin"

shopt -s dotglob
#+END_SRC

** aliases
Here we define all BASH aliases, currently just declarative user packages and pfetch on clear
#+BEGIN_SRC sh :comments org
alias usrpkgs='nvim $HOME/.config/nixpkgs/config.nix && nix-env -iA nixpkgs.usrpkgs'
alias clear='clear;pfetch'
#+END_SRC

** prompt
Here we define my prompt
#+BEGIN_SRC sh :comments org
PS1=$(~/.promptrc.raku)
PS2="$ "
#+END_SRC

** history
Here we make the up and down arrows complete from history instead of just scrolling through it
#+BEGIN_SRC sh :comments org
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\e0A": history-search-backward'
bind '"\e0B": history-search-forward'
#+END_SRC

** autostart
Here, we run everything we want to have on start
#+BEGIN_SRC sh :comments org
pfetch
#+END_SRC

* code (promptrc)
:PROPERTIES:
:header-args:      :tangle ../out/.promptrc.raku
:header-args:raku: :shebang "#!/usr/bin/env raku"
:END:

** color helper
#+BEGIN_SRC raku
# helper for adding color to our segments
my Str sub
color (Str $meta)
{
    return "\\033[{$meta}m";
}
#+END_SRC

** constants
#+BEGIN_SRC raku
# here, we define our segments
constant $LEFT_SEGMENTS = "{color("30;104")}\\u\@\\h  \\w {color("00")}";
constant $BOTTOM_SEGMENTS = "\$  ";
constant $RIGHT_FMT = "{color("30;104")}\$? {color("00")}";
constant $RIGHT_SEGMENTS = 'printf "%$((COLUMNS + 16))s\n" "' ~ $RIGHT_FMT ~ '"';
#+END_SRC

** main
#+BEGIN_SRC raku
my Int sub
MAIN ()
{
    print "\$\($RIGHT_SEGMENTS\)$LEFT_SEGMENTS\n$BOTTOM_SEGMENTS";
    return 0;
}
#+END_SRC
