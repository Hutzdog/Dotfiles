#!/usr/bin/env raku
# helper for adding color to our segments
my Str sub
color (Str $meta)
{
    return "\\033[{$meta}m";
}

# here, we define our segments
constant $LEFT_SEGMENTS = "{color("30;104")}\\u\@\\h  \\w {color("00")}";
constant $BOTTOM_SEGMENTS = "\$  ";
constant $RIGHT_FMT = "{color("30;104")}\$? {color("00")}";
constant $RIGHT_SEGMENTS = 'printf "%$((COLUMNS + 16))s\n" "' ~ $RIGHT_FMT ~ '"';

my Int sub
MAIN ()
{
    print "\$\($RIGHT_SEGMENTS\)$LEFT_SEGMENTS\n$BOTTOM_SEGMENTS";
    return 0;
}
