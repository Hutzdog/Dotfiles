#!/bin/bash

# environment variables
# This is where we have a few variables such as the editor and history configurations. We also tell Bash to allow the * wildcard to see dotfiles

EDITOR="nvim"
HISTCONTROL=ignoreboth
PATH="$PATH:$HOME/.emacs.distribs/bin:$HOME/.emacs.distribs/distbin"

shopt -s dotglob

# aliases
# Here we define all BASH aliases, currently just declarative user packages and pfetch on clear

alias usrpkgs='nvim $HOME/.config/nixpkgs/config.nix && nix-env -iA nixpkgs.usrpkgs'
alias clear='clear;pfetch'

# prompt
# Here we define my prompt

PS1=$(~/.promptrc.raku)
PS2="$ "

# history
# Here we make the up and down arrows complete from history instead of just scrolling through it

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\e0A": history-search-backward'
bind '"\e0B": history-search-forward'

# autostart
# Here, we run everything we want to have on start

pfetch
