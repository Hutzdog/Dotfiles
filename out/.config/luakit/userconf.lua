local settings = require "settings";

local modes = require "modes"

modes.remap_binds("normal", {
    {"<Mouse8>", "<Shift-h>", true},
    {"<Mouse9>", "<Shift-l>", true},
})

local engines = settings.window.search_engines
engines.default = duckduckgo
