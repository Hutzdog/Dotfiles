local theme = {}

-- Vars
local base03 = "#002b36"
local base02 = "#073642"
local base01 = "#586e75"
local base00 = "#657b83"
local base0  = "#839496"
local base1  = "#93a1a1"
local base2  = "#eee8d5"
local base3  = "#fdf6e3"

theme.font = "12px SauceCodePro Nerd Font Mono"
theme.fg   = base0
theme.bg   = base03

theme.warning_fg = base1
theme.warning_bg = base02

theme.notif_fg = base1
theme.notif_bg = base02

theme.menu_fg = base0
theme.menu_bg = base03
theme.menu_selected_fg = base1
theme.menu_selected_bg = base02
theme.menu_title_bg = base1
theme.menu_primary_title_fg = base1
theme.menu_primary_title_bg = base02

theme.menu_disabled_fg = base1
theme.menu_disabled_bg = base03
theme.menu_enabled_fg = theme.menu_fg
theme.menu_enabled_bg = theme.menu_bg
theme.menu_active_fg = base1
theme.menu_active_bg = base02

theme.proxy_active_menu_fg = base1
theme.proxy_active_menu_bg = base02
theme.proxy_inactive_menu_fg = base0
theme.proxy_inactive_menu_bg = base03

theme.sbar_fg = base0
theme.sbar_bg = base03

theme.dbar_fg = base0
theme.dbar_bg = base02
theme.dbar_error_fg = base1

theme.ibar_fg = base0
theme.ibar_bg = base03

theme.tab_fg = base0
theme.tab_bg = base03
theme.tab_hover_bg = base02
theme.tab_ntheme = base0
theme.selected_fg = base1
theme.selected_bg = base02
theme.selected_ntheme = base0
theme.loading_fg = base0
theme.loading_bg = base02
theme.selected_private_tab_bg = base3
theme.private_tab_bg = base1

theme.trust_fg = base0
theme.notrust_fg = base1

theme.ok = { fg = base0, bg = base03 }
theme.warn = { fg = base1, bg = base02 }
theme.error = { fg = base1, bg = base2 }

return theme
